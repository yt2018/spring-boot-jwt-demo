package security;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.util.ObjectUtils;
import security.dao.jpa.MenuJpa;
import security.entity.Menu;
import security.entity.ResurcesUrl;
import security.service.ResurcesUrlSerive;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableCaching
@Slf4j
public class Application {
   static ApplicationContext context;

    public static void main(String[] args) {
        log.info("开始启动了.....启动参数"+JSON.toJSONString(args));
        context = SpringApplication.run(Application.class, args);
        /**
         * 启动完自动打开浏览器
         */
        /*try {
            log.info("启动完成.....正在打开浏览器");
            Runtime.getRuntime().exec("cmd   /c   start   http://localhost:8080");
            log.info("浏览器打开完成....");
        } catch (Exception ex) {
            ex.printStackTrace();
        }*/
        getAllRequestMappingInfo();
        MenuInit();
    }

    public static ApplicationContext getContext() {
        return context;
    }

    /**
     * 扫描所有的路径
     */
    public static void getAllRequestMappingInfo() {
        ResurcesUrlSerive resurcesUrlSerive = getContext().getBean(ResurcesUrlSerive.class);
        //初始化
        resurcesUrlSerive.resetResurcesUrl(-100);
        JSONObject jsonObject = ResurcesUrlSerive.apiResurcesUrl();
        //log.info(jsonObject.toJSONString());
        JSONObject paths = jsonObject.getJSONObject("paths");
        log.info("更新资源库开始....");
        for (String ket:paths.keySet()) {
            ResurcesUrl resurcesUrl = getContext().getBean(ResurcesUrl.class);
            //路径
            resurcesUrl.setPath(ket);
            //请求方式
            JSONObject methodJSONObject = paths.getJSONObject(ket);
            String method = methodJSONObject.keySet().iterator().next();
            resurcesUrl.setMethod(method.toUpperCase());
            // 名字
            JSONObject methodJSONObjectJSONObject = methodJSONObject.getJSONObject(method);
            String summary = methodJSONObjectJSONObject.getString("summary");
            resurcesUrl.setName(summary);
            //查询是否存在
            ResurcesUrl resurcesUrl1 = resurcesUrlSerive.getResurcesUrl(resurcesUrl);
            if (ObjectUtils.isEmpty(resurcesUrl1))  resurcesUrl1= resurcesUrl;
            //描述
            String description = methodJSONObjectJSONObject.getString("description");
            resurcesUrl1.setDescription(description);
            //标记更新状态
            resurcesUrl1.setUpdateStatus(0);
            //创建并保存
            resurcesUrlSerive.createResurcesUrl(resurcesUrl1);
        }
        log.info("更新资源库完成....");
    }
    /**
     * 菜单系统初始化
     */
    public static void MenuInit(){
        String m="[\n" +
                "  {\n" +
                "    \"name\": \"Dashboard\",\n" +
                "    \"url\": \"javascript:;\",\n" +
                "    \"icon\": \"layui-icon layui-icon-home\",\n" +
                "    \"subMenus\": [\n" +
                "      {\n" +
                "        \"name\": \"控制台\",\n" +
                "        \"url\": \"#/console/console1\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"分析页\",\n" +
                "        \"url\": \"#/console/console2\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"欢迎页\",\n" +
                "        \"url\": \"#/console/welcome\"\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"name\": \"系统管理\",\n" +
                "    \"icon\": \"layui-icon layui-icon-set\",\n" +
                "    \"url\": \"javascript:;\",\n" +
                "    \"subMenus\": [\n" +
                "      {\n" +
                "        \"name\": \"用户管理\",\n" +
                "        \"url\": \"#/system/user\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"角色管理\",\n" +
                "        \"url\": \"#/system/role\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"权限管理\",\n" +
                "        \"url\": \"#/system/authorities\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"登录日志\",\n" +
                "        \"url\": \"#/system/loginRecord\"\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"name\": \"模板页面\",\n" +
                "    \"icon\": \"layui-icon layui-icon-template\",\n" +
                "    \"url\": \"javascript:;\",\n" +
                "    \"subMenus\": [\n" +
                "      {\n" +
                "        \"name\": \"表单页\",\n" +
                "        \"url\": \"javascript:;\",\n" +
                "        \"subMenus\": [\n" +
                "          {\n" +
                "            \"name\": \"基础表单\",\n" +
                "            \"url\": \"#/template/form/form-basic\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"name\": \"复杂表单\",\n" +
                "            \"url\": \"#/template/form/form-advance\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"name\": \"分步表单\",\n" +
                "            \"url\": \"#/template/form/form-step\"\n" +
                "          }\n" +
                "        ]\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"表格页\",\n" +
                "        \"url\": \"javascript:;\",\n" +
                "        \"subMenus\": [\n" +
                "          {\n" +
                "            \"name\": \"数据表格\",\n" +
                "            \"url\": \"#/template/table/table-basic\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"name\": \"复杂查询\",\n" +
                "            \"url\": \"#/template/table/table-advance\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"name\": \"树形表格\",\n" +
                "            \"url\": \"#/template/table/table-tree\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"name\": \"左树右表\",\n" +
                "            \"url\": \"#/template/table/table-ltrt\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"name\": \"表格缩略图\",\n" +
                "            \"url\": \"#/template/table/table-img\"\n" +
                "          }\n" +
                "        ]\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"错误页\",\n" +
                "        \"url\": \"javascript:;\",\n" +
                "        \"subMenus\": [\n" +
                "          {\n" +
                "            \"name\": \"500\",\n" +
                "            \"url\": \"#/template/error/error-500\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"name\": \"404\",\n" +
                "            \"url\": \"#/template/error/error-404\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"name\": \"403\",\n" +
                "            \"url\": \"#/template/error/error-403\"\n" +
                "          }\n" +
                "        ]\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"登录页\",\n" +
                "        \"url\": \"javascript:;\",\n" +
                "        \"subMenus\": [\n" +
                "          {\n" +
                "            \"name\": \"登录一\",\n" +
                "            \"url\": \"./components/login/login.html\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"name\": \"登录二\",\n" +
                "            \"url\": \"./components/login/login2.html\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"name\": \"登录三\",\n" +
                "            \"url\": \"./components/login/login3.html\"\n" +
                "          }\n" +
                "        ]\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"个人中心\",\n" +
                "        \"url\": \"#/template/user-info\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"文件管理\",\n" +
                "        \"url\": \"#/template/file/fileMain\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"空白页\",\n" +
                "        \"url\": \"#/other/empty\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"路由传参\",\n" +
                "        \"url\": \"#/other/routerDemo\"\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"name\": \"扩展组件\",\n" +
                "    \"icon\": \"layui-icon layui-icon-app\",\n" +
                "    \"url\": \"javascript:;\",\n" +
                "    \"subMenus\": [\n" +
                "      {\n" +
                "        \"name\": \"下拉菜单\",\n" +
                "        \"url\": \"#/plugin/dropdown\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"消息通知\",\n" +
                "        \"url\": \"#/plugin/notice\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"admin风格弹窗\",\n" +
                "        \"url\": \"#/plugin/dialog\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"鼠标右键菜单\",\n" +
                "        \"url\": \"#/plugin/contextMenu\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"更多扩展\",\n" +
                "        \"url\": \"#/plugin/other\"\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"name\": \"LayUI组件\",\n" +
                "    \"icon\": \"layui-icon layui-icon-release\",\n" +
                "    \"url\": \"javascript:;\",\n" +
                "    \"subMenus\": [\n" +
                "      {\n" +
                "        \"name\": \"组件演示\",\n" +
                "        \"url\": \"#/other/iframe/id=laydemo\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"layui文档\",\n" +
                "        \"url\": \"#/other/iframe/id=layui\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"layer弹窗组件\",\n" +
                "        \"url\": \"#/other/iframe/id=layer\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"laydate日期组件\",\n" +
                "        \"url\": \"#/other/iframe/id=laydate\"\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"name\": \"多级菜单\",\n" +
                "    \"url\": \"javascript:;\",\n" +
                "    \"icon\": \"layui-icon-unlink\",\n" +
                "    \"subMenus\": [\n" +
                "      {\n" +
                "        \"name\": \"二级菜单\",\n" +
                "        \"url\": \"javascript:;\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"name\": \"二级菜单\",\n" +
                "        \"url\": \"javascript:;\",\n" +
                "        \"subMenus\": [\n" +
                "          {\n" +
                "            \"name\": \"三级菜单\",\n" +
                "            \"url\": \"javascript:;\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"name\": \"三级菜单\",\n" +
                "            \"url\": \"javascript:;\",\n" +
                "            \"subMenus\": [\n" +
                "              {\n" +
                "                \"name\": \"四级菜单\",\n" +
                "                \"url\": \"javascript:;\"\n" +
                "              },\n" +
                "              {\n" +
                "                \"name\": \"四级菜单\",\n" +
                "                \"url\": \"javascript:;\",\n" +
                "                \"subMenus\": [\n" +
                "                  {\n" +
                "                    \"name\": \"五级菜单\",\n" +
                "                    \"url\": \"javascript:;\"\n" +
                "                  },\n" +
                "                  {\n" +
                "                    \"name\": \"百度一下\",\n" +
                "                    \"url\": \"#/other/iframe/id=baidu2\"\n" +
                "                  }\n" +
                "                ]\n" +
                "              }\n" +
                "            ]\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    ]\n" +
                "  },\n" +
                "  {\n" +
                "    \"name\": \"一级菜单\",\n" +
                "    \"icon\": \"layui-icon-unlink\",\n" +
                "    \"url\": \"#/other/iframe/id=baidu\"\n" +
                "  }\n" +
                "]";

        MenuJpa bean = getContext().getBean(MenuJpa.class);
        //判断是否是空
        if(bean.count()>0){
            List<Menu> menuList = bean.findAll();
            log.info(JSON.toJSONString(menuList));
            return;
        }
        JSONArray objects = JSON.parseArray(m);
        List<Menu> menus = menuList(objects);
        bean.saveAll(menus);


    }

    public static List<Menu> menuList(JSONArray array){
        List<Menu> menus = new ArrayList<>();
        if(ObjectUtils.isEmpty(array))return null;
        for (int i = 0; i < array.size(); i++) {
            JSONObject jsonObject = array.getJSONObject(i);
            Menu menu = new Menu();
            menu.setName(jsonObject.getString("name"));
            menu.setUrl(jsonObject.getString("url"));
            if(jsonObject.containsKey("icon")) menu.setIcon(jsonObject.getString("icon"));
            if(jsonObject.containsKey("subMenus")){

                JSONArray subMenus = jsonObject.getJSONArray("subMenus");
                List<Menu> menus1 = menuList(subMenus);
                List<Menu> menus2 = new ArrayList<>();
                for (int j = 0; j < menus1.size(); j++) {
                    Menu menu1 = menus1.get(j);
                    menu1.setParent(menu);
                    menus2.add(menu1);
                }
                menu.setSubMenus(menus2);
            }
            menus.add(menu);
        }
        return menus;
    }

}