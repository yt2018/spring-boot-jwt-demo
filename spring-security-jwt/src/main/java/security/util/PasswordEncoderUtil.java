package security.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class PasswordEncoderUtil {
    /**
     * 密码加密方式
     */
    private static PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    /**
     * 对密码使用BCryptPasswordEncoder加密方式进行加密
     */
    public static String passwordEncoder(String password) {
        return passwordEncoder.encode(password);
    }

    /**
     *
     * 功能描述: 密码匹配
     *
     * @param:       passwordDB 数据库密码
     * @param:       password 用户密码
     * @return:
     * @auther: YiTong
     * @date: 2019/2/17 11:44
     */
    public static boolean passwordMatches(String passwordDB, String password) {
        return passwordEncoder.matches(password,passwordDB);
    }

}
