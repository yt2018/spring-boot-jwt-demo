package security.service;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import security.dao.jpa.ResurcesUrlJpa;
import security.entity.ResurcesUrl;
import security.util.ApiMotest;

import java.util.Optional;

/**
 * @Auther: YiTong
 * @Date: 2019/2/21 20:08
 * @Description:
 */
@Service
public class ResurcesUrlSerive {
    @Autowired
    ResurcesUrlJpa resurcesUrlJpa;

    /**
     * 查询资源是否存在
     * @param resurcesUrl
     * @return
     */
    public ResurcesUrl getResurcesUrl(ResurcesUrl resurcesUrl){
        Example<ResurcesUrl> example = Example.of(resurcesUrl);
        Optional<ResurcesUrl> one = resurcesUrlJpa.findOne(example);
        if(ObjectUtils.isEmpty(one))return null;
        return one.get();
    }
    /**
     * 创建资源
     */
    public ResurcesUrl createResurcesUrl(ResurcesUrl resurcesUrl){

        return resurcesUrlJpa.save(resurcesUrl);
    }
    /**
     * 更新资源 标记是否存在
     */
    public int updateByStatusWhereId(Integer id,Integer status){

        return resurcesUrlJpa.updateByStatusWhereId(id,status);
    }
    /**
     * 初始化启动资源
     */
    public int resetResurcesUrl(Integer status){

        return resurcesUrlJpa.updateByStatusAll(status);
    }
    public static JSONObject apiResurcesUrl(){
        String getjson = ApiMotest.sendPost("http://localhost:8080/swagger-api-v2", "GET");
        return JSON.parseObject(getjson);
    }
}
