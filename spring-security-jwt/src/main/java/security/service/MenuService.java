package security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import security.dao.jpa.MenuJpa;
import security.entity.Menu;

import java.util.List;

/**
 * 菜单按钮
 * @Auther: YiTong
 * @Date: 2019/3/2 02:34
 * @Description:
 */
@Service
public class MenuService {
    @Autowired
    MenuJpa menuJpa;
    @Cacheable(cacheNames = "Menu")
    public List<Menu> getMenuJpa() {
        return menuJpa.findAll();
    }
}
