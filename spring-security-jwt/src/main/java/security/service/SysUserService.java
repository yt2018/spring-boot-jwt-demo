package security.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import security.dao.jpa.SysUserDao;
import security.entity.SysUser;
import security.util.PasswordEncoderUtil;

/**
 * @Auther: YiTong
 * @Date: 2019/2/18 14:09
 * @Description:
 */
@Slf4j
@Service
@CacheConfig(cacheNames = "SysUser")
public class SysUserService implements UserDetailsService {
    @Autowired
    SysUserDao sysUserDao;
    @Autowired
    CacheManager cacheManager;


    /**
     * 根据用户名查找
     *
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */

    @Cacheable(key = "#username",unless = "#result == null")
    public UserDetails loadUserByUsername(String username) {
        log.info(username+"账号查询");
        SysUser firstByUsername = sysUserDao.findFirstByUsername(username);
        if (ObjectUtils.isEmpty(firstByUsername)) {
            clearUserCacheManager(username);
            throw new UsernameNotFoundException("账号不存在,或密码错误");
        }else {
            return firstByUsername;
        }
    }

    /**
     * 注册用户
     *
     */
    public SysUser createUser(String username, String password) {
        //同步注册防止注册冲击
        synchronized (username) {
            SysUser firstByUsername = sysUserDao.findFirstByUsername(username);
            if (!ObjectUtils.isEmpty(firstByUsername)) {
                throw new UsernameNotFoundException("账号已存在");
            }else {
                //清除缓存
                clearUserCacheManager(username);
                SysUser sysUser = new SysUser(username, PasswordEncoderUtil.passwordEncoder(password));
                return sysUserDao.saveAndFlush(sysUser);
            }
        }
    }
    /**
     * 更据id查找用户
     */
    @Cacheable(value = "SysUser",key = "'user'+#id")
    public SysUser getUser(Long id){
        SysUser sysUser = sysUserDao.findById(id).get();
        if (ObjectUtils.isEmpty(sysUser)) {
            clearUserCacheManager("user"+id);
            throw new UsernameNotFoundException("账号不存在");
        }else {
            return sysUser;
        }
    }
    /**
     * 登录
     *
     * @return
     */
    public UserDetails login(String username, String password) {

        UserDetails firstByUsername = loadUserByUsername(username);
        String password1 = firstByUsername.getPassword();
        /**
         * 密码对比
         */
        if (!PasswordEncoderUtil.passwordMatches(password1, password)) {
            throw new UsernameNotFoundException("账号不存在,或密码错误");
        }else{
            return firstByUsername;
        }
    }

    /**
     * 修改用户,更新用户信息
     *
     * @param userDetails
     */
    public void updateUser(UserDetails userDetails) {

    }

    /**
     * 删除用户
     *
     * @param s
     */
    public void deleteUser(String s) {

    }

    /**
     * 修改密码
     *
     * @param s
     * @param s1
     */
    public void changePassword(String s, String s1) {

    }

    /**
     * 用户退出
     *
     * @param s
     * @return
     */
    public boolean userExists(String s) {
        return false;
    }
    /**
     * 清楚缓存
     */
    public void clearUserCacheManager(String username){

        Cache sysUser = cacheManager.getCache("SysUser");

        sysUser.evict(username);



    }

}
