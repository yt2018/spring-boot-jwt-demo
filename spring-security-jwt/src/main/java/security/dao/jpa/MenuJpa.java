package security.dao.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import security.entity.Menu;

/**
 * @Auther: YiTong
 * @Date: 2019/2/24 22:06
 * @Description:
 */
@Repository
public interface MenuJpa extends JpaRepository<Menu,Integer> {
}
