package security.dao.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import security.entity.ResurcesUrl;

/**
 * @Auther: YiTong
 * @Date: 2019/2/21 20:10
 * @Description:
 */
public interface ResurcesUrlJpa extends JpaRepository<ResurcesUrl,Integer> {
    /**
     * 单个资源检查
     * @param id
     * @param status
     * @return
     */
    @Transactional
    @Modifying
    @Query("update ResurcesUrl r set r.updateStatus=:status where r.id=:id")
    int updateByStatusWhereId(@Param("id") Integer id, @Param("status") Integer status);

    /**
     * 初始化资源
     * @param status
     * @return
     */
    @Transactional
    @Modifying
    @Query("update ResurcesUrl r set r.updateStatus=:status")
    int updateByStatusAll(@Param("status") Integer status);
}
