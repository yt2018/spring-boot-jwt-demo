package security.dao.jpa;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import security.entity.SysUser;

/**
 * @Auther: YiTong
 * @Date: 2019/2/18 14:32
 * @Description:
 */
@Repository
@CacheConfig(cacheNames = "SysUser")
public interface SysUserDao extends JpaRepository<SysUser, Long> {
    /**
     * 查找用户
     * @param username
     * @return
     */
    @Cacheable
    SysUser findFirstByUsername(String username);

}
