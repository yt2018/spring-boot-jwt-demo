package security.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import security.entity.SysUser;

/**
 * @Auther: YiTong
 * @Date: 2019/2/26 13:25
 * @Description:
 */
public interface SysUserMapper extends BaseMapper<SysUser> {
}
