package security.entity;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import security.BaseAppClass;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 *
 * 功能描述:  角色实体类
 *
 * @param:
 * @return:
 * @auther: YiTong
 * @date: 2019/2/10 23:40
 */
@Component
@Entity
@Data
public class Role implements BaseAppClass,GrantedAuthority {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    /**
     * 角色名称
     */
    private String name;

    @Override
    public Class<?> getEntity() {
        return this.getClass();
    }

    @Override
    public String getAuthority() {
        return name;
    }
}
