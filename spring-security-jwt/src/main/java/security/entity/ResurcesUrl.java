package security.entity;

import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;

/**
 * url地址扫描写入数据库
 * 本数据禁止修改
 * @Auther: YiTong
 * @Date: 2019/2/16 22:52
 * @Description:
 * 默认是单例模式，即scope="singleton"。另外scope还有prototype、request、session、global session作用域。
 */
@Component

@Scope("prototype")
@Data
@Entity
public class ResurcesUrl {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    /**
     * 路径
     * 禁止修改
     */
    private String path;
    /**
     * 请求方式
     * 禁止修改
     */
    private String method;
    /**
     * 菜单名字
     * 禁止修改
     */
    private String name;
    /**
     * 菜单名字
     * 禁止修改
     */
    @Column(columnDefinition = "text")
    private String description;
    /**
     * 地址状态状态
     * 初始化统一修改为-100
     * 原因 检查该链接是否存在,检查状态状态正常 0
     * 新增为1
     * 修改为2
     */
    private Integer updateStatus;
    /**
     *  是否限制访问状态
     */
    private Integer status;
    
}
