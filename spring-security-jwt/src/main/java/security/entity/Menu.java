package security.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

/**
 * 树形菜单
 * @Auther: YiTong
 * @Date: 2019/2/24 15:22
 * @Description:
 */
@Getter
@Setter
@Entity
public class Menu {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    /**
     * 菜单名字
     */
    private String name;
    /**
     *  url
     */
    private String url;
    /**
     *  图标
     */
    private String icon;
    /**
     * 父id
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn
    private Menu parent;
    /**
     * 子集合
     */
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER, mappedBy="parent")
     private Collection<Menu> subMenus;


}
