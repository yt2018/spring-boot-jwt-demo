package security;

/**
 *
 * 功能描述:
 * 查找类接口
 * 使用说明
 * 前言:
 *  以前使用  java的反射
 *  	无法事先知道使用者将加载什么类，而必须让使用者指定类名称以加载类，可以使用 Class 的静态 forName() 方法实现动态加载类。
 *  Class.forName使用前提
 *  	1.知道明确的包路径   -->3
 *  	2.大小写不能出错
 *  	3.更换包路径很麻烦
 * 使用spring的单例返回的Obj
 * 所以定义一个类接口获取类的信息
 * 使用说明:
 * 方法类上加Spring一下的注解
 * @Named 不仅可以用于依赖注入来指定注入的Bean的标识符，还可以用于定义Bean。
 * 即注解在类型上表示定义Bean，注解在非类型上（如字段）表示指定依赖注入的Bean标识符。
 * @Component 注解，代表它成了一个组件，如果标识符不写，那么默认就是类名，一般推荐你还是自己写个名字上去比较好。
 * @Repository @Component 扩展，被@Repository注解的POJO类表示DAO层实现，从而见到该注解就想到DAO层实现，使用方式和@Component相同；
 * @Service @Component扩展，被@Service注解的POJO类表示Service层实现，从而见到该注解就想到Service层实现，使用方式和@Component相同；
 * @Controller： @Component扩展，被@Controller注解的类表示Web层实现，从而见到该注解就想到Web层实现，使用方式和@Component相同；
 * 如果以上注解name有自定义的名字使用自定义的名字
 * 如果以上注解name没自定义的名字则是 类名(注意第一个字母小写)
 *
 * 例子:
 * 第一步
 * public class Role implements BaseAppClass
 *
 * 		实现方法
 *    @Override
 *     public Class<?> getEntity() {
 *         return this.getClass();
 *     }
 *     //获取类
 *          Class<?> entity = appClass.getEntity();
 *          //获取类名(不带包路径)
 *          String simpleName = entity.getSimpleName();
 *    BaseAppClass appClass = Application.getContext().getBean("beanId名字", BaseAppClass.class);
 * @auther: YiTong
 * @date: 2019/2/17 10:41
 */
public interface BaseAppClass {
	/**
	 * 返回类
	 * @return
	 */
	Class<?> getEntity();
}