package security.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import security.config.AppConfigUrl;
import security.config.JwtUtil;
import security.entity.SysUser;
import security.service.MenuService;
import security.service.SysUserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Map;


@Api(value = "测试页面",description = "除了登录页面,其余的页面需要令牌登录",consumes = "yitong")
@RestController
@Slf4j
public class HelloController {
    /**
     * 配置文件
     */
    @Autowired
    public AppConfigUrl appConfigUrl;
    /**
     * 用户服务
     */
    @Autowired
    SysUserService sysUserService;
    /**
     * 令牌工具
     */
    @Autowired
    JwtUtil jwtUtil;

    /**
      *
      * 功能描述:   角色测试
      *
      * @param:
      * @return:
      * @auther: YiTong
      * @date: 2019/2/10 22:47
      */
     @ApiOperation(value = "角色测试",notes = "需要`ADMIN_USER`角色",consumes = "yitong")
    @GetMapping(value = "/api/admin",name = "角色测试")
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public @ResponseBody
    Object helloToAdmin(String userId) {
        return "Hello World! You are ADMIN_USER "+userId;
    }

    /**
     *
     * 功能描述:  无角色访问
     *
     * @param:
     * @return:
     * @auther: YiTong
     * @date: 2019/2/10 23:26
     */
    @ApiOperation(value = "普通用户测试",notes = "无需角色",consumes = "yitong")
    @GetMapping("/api/hello")
    public @ResponseBody
    Object hello(String userId) {
        return "Hello World! 你可以访问了!"+userId;
    }
    /**
     *
     * 功能描述: 后生成jwt令牌相应到header里
     * jwt令牌有利于多台机器运行避免session回话过期
     *
     * @param:
     * @return:
     * @auther: YiTong
     * @date: 2019/2/10 22:40
     */
    @ApiOperation(value = "登录测试",notes = "默认账号是:root@admin.com\r\n密码:root@admin.com\r\n ```json \r\n"  +
            " {\n" +
            "  \"password\": \"root@admin.com\",\n" +
            "  \"username\": \"root@admin.com\"\n" +
            "} " +
            "\r\n```",consumes = "yitong")
    @PostMapping("/login")
    public Map<Object, Object> login(HttpServletResponse response, HttpServletRequest request,
                                     @Validated @RequestBody  Account account){
        Map<Object, Object> jsonResult = null;
        UserDetails user = sysUserService.login(account.username, account.password);
        String jwt = jwtUtil.generateToken(user,response,request);
        jsonResult = JsonResult.Result(200, "登陆成功");
        jsonResult.put(appConfigUrl.getHEADER_STRING(), jwt);
        jsonResult.put("account", account);

        return jsonResult;
    }
    @Autowired
    MenuService menuService;

    /**
     * 菜单列表
     * @return
     */
    @ApiOperation(value = "菜单列表",notes = "菜单列表单tree",consumes = "yitong")
    @PostMapping("/api/getMenu")
    public Object getMenu(){


        return menuService.getMenuJpa();
    }
    /**
     *
     * 功能描述: 后生成jwt令牌相应到header里
     * jwt令牌有利于多台机器运行避免session回话过期
     *
     * @param:
     * @return:
     * @auther: YiTong
     * @date: 2019/2/10 22:40
     */
    @ApiOperation(value = "注册测试",notes = "默认账号是无角色",consumes = "yitong")
    @PostMapping("/logup")
    public Map<Object, Object> logup(HttpServletResponse response, HttpServletRequest request,
                                     @Validated @RequestBody  Account account){
        Map<Object, Object> jsonResult = null;
        SysUser user = sysUserService.createUser(account.username,account.password);
        String jwt = jwtUtil.generateToken(user,response,request);
        jsonResult = JsonResult.Result(200, "注册成功");
        jsonResult.put(appConfigUrl.getHEADER_STRING(), jwt);
        jsonResult.put("account", account);

        return jsonResult;
    }

     @ApiModel(value = "账号")
    public static class Account {

        @Email(message = "请输入正确的邮箱")
        public String username;
        @NotNull(message = "密码不能为空")
        @NotBlank(message = "密码不能为空")
        public String password;
    }

}