package security;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.databene.contiperf.PerfTest;
import org.databene.contiperf.junit.ContiPerfRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import security.dao.mapper.SysUserMapper;
import security.entity.SysUser;

import java.util.List;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@WithMockUser(username="test",password = "test",authorities = "ROOT")
/*@WebMvcTest*/
@SpringBootTest
@Slf4j
public class TestApplicationTests {
    @Autowired
    private WebApplicationContext webApplicationContext;

    public MockMvc mvc;

    @Rule
    public ContiPerfRule contiPerfRule = new ContiPerfRule();
    @Before
    public void before() {
        //获取mockmvc对象实例
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
    @Test
    @PerfTest(invocations = 10,threads = 3)
    public void test(){
        log.info("测试了....@PerfTest(invocations = 10次,threads = 3个线程)");
    }
    @Autowired
    private SysUserMapper sysUserMapper;

    /**
     * mybatis plus测试
     */
    @Test
    public void testSelect() {
        System.out.println(("----- selectAll method test ------"));
        List<SysUser> userList = sysUserMapper.selectList(null);
        //Assert.assertEquals(5, userList.size());
        //userList.forEach(System.out::println);
        IPage page =new Page<SysUser>(0,2);
        QueryWrapper<SysUser> eq = new QueryWrapper<SysUser>().eq("1", 1);

        sysUserMapper.selectPage(page, eq);
    }
}

