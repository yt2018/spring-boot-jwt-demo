package security;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.databene.contiperf.PerfTest;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import security.controller.HelloController;
@Slf4j
public class BaseControllerTest extends TestApplicationTests{

    @Test
    @PerfTest(invocations = 100,threads = 30)
    public void index(){
        try {
            ResultActions perform = mvc.perform(MockMvcRequestBuilders.post("/").accept(MediaType.APPLICATION_JSON));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    @PerfTest(invocations = 100,threads = 30)
    public void getUsername(){
        HelloController.Account account = new HelloController.Account();
        account.username="root@admin.com";
        account.password="root@admin.com";
        String content = JSON.toJSONString(account);
        ResultActions perform = null;
        try {
            perform = mvc.perform(MockMvcRequestBuilders.post("/login")
                    .contentType("application/json; charset=UTF-8").content(content)
                    .accept(MediaType.APPLICATION_JSON));
            MvcResult mvcResult = perform.andReturn();
            log.info("请求结果:"+ mvcResult.getResponse().getContentAsString());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}