package security.controller;

import lombok.extern.slf4j.Slf4j;
import org.databene.contiperf.PerfTest;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import security.TestApplicationTests;

import java.util.UUID;

/**
 * @Auther: YiTong
 * @Date: 2019/2/23 13:57
 * @Description:
 */
@Slf4j
public class HelloControllerTest extends TestApplicationTests {

    @Test
    @PerfTest(invocations = 100,threads = 100)
    public void login() {
        try {
            ResultActions perform = mvc.perform(MockMvcRequestBuilders.post("/login")
                    .contentType(MediaType.APPLICATION_JSON).content("{\n" +
                            "  \"password\": \"1@qq.com\",\n" +
                            "  \"username\": \"1@qq.com\"\n" +
                            "}").accept(MediaType.APPLICATION_JSON));
            MvcResult mvcResult = perform.andReturn();
            log.info("请求结果:"+ mvcResult.getResponse().getContentAsString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @PerfTest(invocations = 100,threads = 10)
    public void logup() {
        //logup
        try {
            UUID uuid = UUID.randomUUID();
            String s = uuid.toString().replaceAll("-", "");
            ResultActions perform = mvc.perform(MockMvcRequestBuilders.post("/logup")
                    .contentType(MediaType.APPLICATION_JSON).content("{\n" +
                            "  \"password\": \""+ s +"@qq.com\",\n" +
                            "  \"username\": \""+s+"@qq.com\"\n" +
                            "}").accept(MediaType.APPLICATION_JSON));
            MvcResult mvcResult = perform.andReturn();
            log.info("请求结果:"+ mvcResult.getResponse().getContentAsString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}